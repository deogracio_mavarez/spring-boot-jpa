package com.springboot.jpadata.app.springbootjpadata.controller;

import com.springboot.jpadata.app.springbootjpadata.entity.Client;
import com.springboot.jpadata.app.springbootjpadata.service.ClientService;
import com.springboot.jpadata.app.springbootjpadata.util.paginator.PageRender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Optional;

@Controller
//@RestController
@RequestMapping("/client")
public class ClientController {

    private static final String DEFAUL_TITLE = "titulo";
    private static final String DEFAUL_DANGER = "danger";
    private static final String DEFAUL_INFO = "info";
    private static final String DEFAUL_CLIENT = "client";
    private static final String REDIRECT_TO_LIST = "redirect:../../client/list";


    @Value("${factura.directory.photo}")
    private String directoryPhoto;

    @Autowired
    private ClientService clientService;

    @GetMapping("/lista")
    public Page<Client> getAll(@RequestParam(name = "page", defaultValue = "0") int page) {
        Pageable pageable = PageRequest.of(page, 5);
        return clientService.findAll(pageable);
    }

    @GetMapping("/list")
    public String getAll(@RequestParam(name = "page", defaultValue = "0") int page, Model model) {
        Pageable pageable = PageRequest.of(page, 4);
        Page<Client> clients = clientService.findAll(pageable);
        PageRender<Client> clientPageRender = new PageRender<>("../client/list", clients);

        model.addAttribute(DEFAUL_TITLE, "lista de client");
        model.addAttribute("clientes", clients);
        model.addAttribute("page", clientPageRender);

        return "/listar";
    }

    @GetMapping("/formClient")
    public String create(Map<String, Object> model) {
        Client client = new Client();
        model.put(DEFAUL_TITLE, "Formulario para crear");
        model.put(DEFAUL_CLIENT, client);
        return "/formClient";
    }

    @GetMapping("/formClient/{id}")
    public String edit(@PathVariable(value = "id") Integer id, Map<String, Object> model, RedirectAttributes flash) {

        Optional<Client> client = findClientById(id);

        if (id > 0) {
            if (!client.isPresent()) {
                flash.addFlashAttribute(DEFAUL_DANGER, "no se encontro el id en BD");
                return REDIRECT_TO_LIST;
            }
        } else {
            flash.addFlashAttribute(DEFAUL_DANGER, "el id no puede ser 0");
            return REDIRECT_TO_LIST;
        }
        model.put(DEFAUL_CLIENT, client.get());
        model.put(DEFAUL_TITLE, "editar cliente");
        return "/formClient";
    }

    @GetMapping("/eliminar/{id}")
    public String delete(@PathVariable(value = "id") Integer id, RedirectAttributes flash) {
        Optional<Client> client = clientService.findOne(id);
        if (client.isPresent()) {
            clientService.delete(id);
            flash.addFlashAttribute("success", "cliente eliminado con exito");

            File file = Paths.get(directoryPhoto).resolve(client.get().getPhoto()).toAbsolutePath().toFile();
            if (file.exists() && file.canRead()) {
                if (file.delete()) {
                    flash.addFlashAttribute(DEFAUL_INFO, "Foto Eliminada con exito");
                } else {
                    flash.addFlashAttribute(DEFAUL_DANGER, "Foto no fue Eliminada con exito");
                }
            }
            return "redirect:../list";
        } else {
            flash.addFlashAttribute(DEFAUL_DANGER, "No se encontro el ID en BD");
            return "redirect:../list";
        }
    }

    private Optional<Client> findClientById(Integer id) {
        return clientService.findOne(id);
    }

    @GetMapping("/ver/{id}")
    public String ver(@PathVariable(value = "id") Integer id, RedirectAttributes flash, Map<String, Object> model) {

        Optional<Client> client = findClientById(id);

        if (!client.isPresent()) {
            flash.addFlashAttribute(DEFAUL_DANGER, "no se encontro el id en BD");
            return REDIRECT_TO_LIST;
        }
        model.put(DEFAUL_CLIENT, client.get());
        model.put(DEFAUL_TITLE, "detalle de cliente: " + client.get().getName());
        return "/ver";
    }

    @PostMapping("/save")
    public String save(@Valid Client client, @RequestParam("file") MultipartFile file, BindingResult bindingResult, Model model, RedirectAttributes flash, SessionStatus status) {
        if (bindingResult.hasErrors()) {
            model.addAttribute(DEFAUL_TITLE, "Formulario para crear");
            return "redirect:list";
        }

        if (!file.isEmpty()) {

            try {
                byte[] bytes = file.getBytes();
                Path completeRute = Paths.get(directoryPhoto + "/" + file.getOriginalFilename());
                Files.write(completeRute, bytes);
                flash.addFlashAttribute("info", "has subido correctamente la foto");

                client.setPhoto(file.getOriginalFilename());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        clientService.create(client);
        flash.addFlashAttribute("success", "cliente creado con exito");
        return "redirect:list";
    }

}
