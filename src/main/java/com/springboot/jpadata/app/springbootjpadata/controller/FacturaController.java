package com.springboot.jpadata.app.springbootjpadata.controller;


import com.springboot.jpadata.app.springbootjpadata.entity.Client;
import com.springboot.jpadata.app.springbootjpadata.entity.Factura;
import com.springboot.jpadata.app.springbootjpadata.entity.Product;
import com.springboot.jpadata.app.springbootjpadata.service.ClientService;
import com.springboot.jpadata.app.springbootjpadata.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Controller
@RequestMapping("/factura")
public class FacturaController {

    private static final String DEFAUL_TITLE = "titulo";
    private static final String DEFAUL_DANGER = "danger";
    private static final String DEFAUL_INFO = "info";
    private static final String DEFAUL_CLIENT = "client";
    private static final String REDIRECT_TO_LIST = "redirect:../../client/list";

    @Autowired
    private ClientService clientService;

    @Autowired
    private ProductService productService;

    @GetMapping("/crear/{idClient}")
    public String createFactura(@PathVariable Integer idClient,
                                RedirectAttributes flash,
                                Map<String, Object> model) {
        Optional<Client> client = clientService.findOne(idClient);
        if(!client.isPresent()){
            flash.addFlashAttribute(DEFAUL_DANGER,"no se enecontro cliente en BD");
            return REDIRECT_TO_LIST;
        }
        Factura factura = new Factura();
        factura.setClient(client.get());
        model.put("factura", factura);
        model.put(DEFAUL_TITLE, "Crear Factura");
        return "factura/formulario";
    }

    @GetMapping(value = "/cargar-productos/{term}", produces = { "application/json" })
    public @ResponseBody
    List<Product> cargarProductos(@PathVariable String term) {
        return productService.findByNombreIsLike(term);
    }

}
