package com.springboot.jpadata.app.springbootjpadata;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class SpringbootJpadataApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootJpadataApplication.class, args);
    }

}
