package com.springboot.jpadata.app.springbootjpadata.service;

import com.springboot.jpadata.app.springbootjpadata.entity.Product;

import java.util.List;

public interface ProductService {

    List<Product> findByNombreIsLike(String item);

}
