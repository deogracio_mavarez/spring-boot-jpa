package com.springboot.jpadata.app.springbootjpadata.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Entity
@Table(name = "facturas")
public class Factura implements Serializable {

    private static final long serialVersionUID = 1905122041950251207L;


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String description;

    private String observaciones;

    @NotNull
    @Column(name = "create_date")
    @Temporal(value = TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-mm-dd")
    private Date createDate;

    @PrePersist
    public void prePersist() {
        this.createDate = new Date();
    }

    @ManyToOne(targetEntity = Client.class, fetch = FetchType.LAZY)
    private Client client;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "id_factura")
    private List<ItemFactura> itemFacturaList;

    public Factura() {

        this.itemFacturaList = new ArrayList<>();

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public List<ItemFactura> getItemFacturaList() {
        return itemFacturaList;
    }

    public void setItemFacturaList(List<ItemFactura> itemFacturaList) {
        this.itemFacturaList = itemFacturaList;
    }

    public void addItem(ItemFactura itemFactura) {
        itemFacturaList.add(itemFactura);
    }

    public Double getTotal() {
        Double total = 0.0;
        for (int i = 0; i < itemFacturaList.size(); i++) {
            total += itemFacturaList.get(i).calcularItem();
        }
        return total;
    }


}
