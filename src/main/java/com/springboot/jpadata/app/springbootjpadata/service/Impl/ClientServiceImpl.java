package com.springboot.jpadata.app.springbootjpadata.service.Impl;

import com.springboot.jpadata.app.springbootjpadata.entity.Client;
import com.springboot.jpadata.app.springbootjpadata.repository.ClientRepository;
import com.springboot.jpadata.app.springbootjpadata.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;


@Service
public class ClientServiceImpl implements ClientService {

    @Autowired
    private ClientRepository clientRepository;


    @Override
    @Transactional(readOnly = true)
    public List<Client> findAll() {
        return (List<Client>) clientRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Client> findAll(Pageable pageable) {
        return clientRepository.findAll(pageable);
    }


    @Override
    @Transactional
    public Client create(Client client) {
        return clientRepository.save(client);
    }

    @Override
    @Transactional
    public Optional<Client> findOne(Integer id) {
        return clientRepository.findById(id);
    }

    @Override
    @Transactional
    public void delete(Integer id) {
        clientRepository.deleteById(id);
    }
}
