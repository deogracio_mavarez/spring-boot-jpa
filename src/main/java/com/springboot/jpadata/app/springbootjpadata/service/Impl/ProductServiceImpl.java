package com.springboot.jpadata.app.springbootjpadata.service.Impl;

import com.springboot.jpadata.app.springbootjpadata.entity.Product;
import com.springboot.jpadata.app.springbootjpadata.repository.ProductRepository;
import com.springboot.jpadata.app.springbootjpadata.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class ProductServiceImpl implements ProductService {


    @Autowired
    private ProductRepository productRepository;


    @Override
    public List<Product> findByNombreIsLike(String item) {
        return productRepository.findByNombre(item);
    }
}
