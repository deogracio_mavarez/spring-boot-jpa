package com.springboot.jpadata.app.springbootjpadata.repository;

import com.springboot.jpadata.app.springbootjpadata.entity.Factura;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FacturaRepository extends JpaRepository<Factura, Long> {

}
